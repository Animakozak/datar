﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class OriginController : MonoBehaviour
{
    public Slider sliderX; 
    public Slider sliderY; 
    public Slider sliderZ; 
    public Slider offset;

    private float scaleX = 1.0f;
    private float scaleY = 1.0f;
    private float scaleZ = 1.0f;
    private List<GameObject> datapoints;
    private List<Vector3> coordinates;

    private float TOLERANCE = 0.01f;
    // Start is called before the first frame update
    void Start()
    {
        datapoints = GetComponent<Plotting>().ScatterPlot;
        coordinates = GetComponent<Plotting>().PlotPoints;
    }

    // Update is called once per frame
    void Update()
    {
        
        transform.localPosition = new Vector3(0,offset.value,0);
//        GetComponent<Plotting>().axisScaleX = scaleX.value;
//        GetComponent<Plotting>().axisScaleY = scaleY.value;
//        GetComponent<Plotting>().axisScaleZ = scaleZ.value;
        
            
        
    }

//    public void ScaleAxisX()
//    {
//        scaleX = sliderX.value;
//        foreach (GameObject datapoint in datapoints)
//        {
//            int i = datapoints.IndexOf(datapoint);
//            var position = coordinates[i];
//            position.x *= scaleX;
//            datapoint.transform.localPosition = position;
//        }
//    }
//    public void ScaleAxisY()
//    {
//        scaleY = sliderY.value;
//        foreach (GameObject datapoint in datapoints)
//        {
//            int i = datapoints.IndexOf(datapoint);
//            var position = coordinates[i];
//            position.y *= scaleY;
//            datapoint.transform.localPosition = position;
//        }
//    }
//    public void ScaleAxisZ()
//    {
//        scaleZ = sliderZ.value;
//        foreach (GameObject datapoint in datapoints)
//        {
//            int i = datapoints.IndexOf(datapoint);
//            var position = coordinates[i];
//            position.z *= scaleZ;
//            datapoint.transform.localPosition = position;
//        }
//    }

    public void ScaleAxis()
    {
        scaleX = sliderX.value;
        scaleY = sliderY.value;
        scaleZ = sliderZ.value;
        foreach (GameObject datapoint in datapoints)
        {
            int i = datapoints.IndexOf(datapoint);
            var position = coordinates[i];
            position.x *= scaleX;
            position.y *= scaleY;
            position.z *= scaleZ;
            datapoint.transform.localPosition = position;
        }
    }
}
