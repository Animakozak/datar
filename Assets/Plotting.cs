﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using UnityEngine;

public class Plotting : MonoBehaviour
{
//	public GameObject spawnpoint;
	public GameObject point;
	public string filepath;
	
//	private List<Vector3> values;
	private List<Vector3> valuesStr;
	private List<string> desc;
	public List<GameObject> ScatterPlot;
	public List<Vector3> PlotPoints;

//	[HideInInspector]
	public float axisScaleX = 1.0f;
//	[HideInInspector]
	public float axisScaleY = 1.0f;
//	[HideInInspector]
	public float axisScaleZ = 1.0f;
	
	
	// Use this for initialization
	void Start ()
	{
		valuesStr = new List<Vector3>();
		ScatterPlot = new List<GameObject>();
		TextAsset txt = (TextAsset)Resources.Load(filepath, typeof(TextAsset));
		string filecontent = txt.ToString();

		CultureInfo ci = (CultureInfo)CultureInfo.CurrentCulture.Clone();
		ci.NumberFormat.CurrencyDecimalSeparator = ".";
		string[] lines= filecontent.Split(new[] { Environment.NewLine }, StringSplitOptions.None);
		foreach (string vals in lines)
		{
			
			string[] values = vals.Split(',');
			valuesStr.Add(new Vector3(
				float.Parse(values[0], CultureInfo.InvariantCulture.NumberFormat),
				float.Parse(values[1], CultureInfo.InvariantCulture.NumberFormat),
				float.Parse(values[2], CultureInfo.InvariantCulture.NumberFormat)));
			
		}


		Vector3 minValue = MinMax(valuesStr)[0];
		Vector3 maxValue = MinMax(valuesStr)[1];
		

		foreach (var VARIABLE in valuesStr)
		{
			Vector3 pos = new Vector3(
				(VARIABLE.x - MinMaxX(valuesStr)[0].x) / (MinMaxY(valuesStr)[1].x - MinMaxX(valuesStr)[0].x),
				(VARIABLE.y - MinMaxY(valuesStr)[0].y) / (MinMaxY(valuesStr)[1].y - MinMaxY(valuesStr)[0].y),
				(VARIABLE.z - MinMaxZ(valuesStr)[0].z) / (MinMaxZ(valuesStr)[1].z - MinMaxZ(valuesStr)[0].z)
			);
			
			GameObject datapoint = Instantiate(point, transform);
			datapoint.transform.localPosition = pos;
			ScatterPlot.Add(datapoint);
			PlotPoints.Add(pos);
		}
	}
	
	// Update is called once per frame
	void Update () {
//		UpdatePosition();
	}

	Vector3[] MinMax(List<Vector3> arr)
	{
		Vector3 minVector = Vector3.positiveInfinity; 
		Vector3 maxVector = Vector3.zero;
		for(int i = 0; i < arr.Count; i++){
			minVector = (arr[i].magnitude < minVector.magnitude) ?  arr[i] : minVector;
			maxVector = (arr[i].magnitude > maxVector.magnitude) ?  arr[i] : maxVector;
		}
//		Debug.Log(minVector);
//		Debug.Log(maxVector);
		return new[] {minVector, maxVector};
	}
	
	Vector3[] MinMaxX(List<Vector3> arr)
	{
		Vector3 minVector = Vector3.positiveInfinity; 
		Vector3 maxVector = Vector3.zero;
		for(int i = 0; i < arr.Count; i++){
			minVector = (arr[i].x < minVector.x) ?  arr[i] : minVector;
			maxVector = (arr[i].x > maxVector.x) ?  arr[i] : maxVector;
		}
//		Debug.Log(minVector);
//		Debug.Log(maxVector);
		return new[] {minVector, maxVector};
	}
	
	Vector3[] MinMaxY(List<Vector3> arr)
	{
		Vector3 minVector = Vector3.positiveInfinity; 
		Vector3 maxVector = Vector3.zero;
		for(int i = 0; i < arr.Count; i++){
			minVector = (arr[i].y < minVector.y) ?  arr[i] : minVector;
			maxVector = (arr[i].y> maxVector.y) ?  arr[i] : maxVector;
		}
//		Debug.Log(minVector);
//		Debug.Log(maxVector);
		return new[] {minVector, maxVector};
	}
	
	Vector3[] MinMaxZ(List<Vector3> arr)
	{
		Vector3 minVector = Vector3.positiveInfinity; 
		Vector3 maxVector = Vector3.zero;
		for(int i = 0; i < arr.Count; i++){
			minVector = (arr[i].z < minVector.z) ?  arr[i] : minVector;
			maxVector = (arr[i].z> maxVector.z) ?  arr[i] : maxVector;
		}
//		Debug.Log(minVector);
//		Debug.Log(maxVector);
		return new[] {minVector, maxVector};
	}

	void UpdatePosition()
	{
		foreach (GameObject datapoint in ScatterPlot)
		{
			datapoint.transform.position = new Vector3(
				datapoint.transform.position.x * axisScaleX, 
				datapoint.transform.position.y * axisScaleY,
				datapoint.transform.position.z * axisScaleZ);
			Debug.Log(datapoint.transform.position);

		}
	}
}
