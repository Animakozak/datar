﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ButtonToggle : MonoBehaviour
{
    private bool Toggled;
    
    public void Toggle()
    {
        if (Toggled)
        {
            Toggled = false;
        }
        else
        {
            Toggled = true;
        }

        gameObject.SetActive(Toggled);
    }
}
