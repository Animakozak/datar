﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class ModeSelector : MonoBehaviour
{
    [SerializeField] private string sandboxScene = "sandbox";
    [SerializeField] private string educationScene = "education";
    [SerializeField] private string experimentsScene = "labs";
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void LoadSandboxScene()
    {
        SceneManager.LoadScene(sandboxScene);
    }
}
